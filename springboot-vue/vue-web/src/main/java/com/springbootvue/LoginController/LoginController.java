package com.springbootvue.LoginController;

import com.alibaba.fastjson.JSONObject;
import com.springbootvue.service.LoginService.LoginService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author yhz
 * @Data 20:16 2020/1/24
 */
@RestController
@RequestMapping("/login")
public class LoginController {
    @Resource
    LoginService service;

    @ApiOperation(value = "登陆接口",notes = "登陆")
    @ApiImplicitParam(name = "user",value = "user测试")
    @RequestMapping(value = "/{info}",method = RequestMethod.GET)
    public String login(@PathVariable Map map){
        JSONObject info = JSONObject.parseObject(map.get("info").toString());
        Map login = service.login(info);
        if(login!=null){
            return "success";
        }else{
            return "false";
        }
    }
}
