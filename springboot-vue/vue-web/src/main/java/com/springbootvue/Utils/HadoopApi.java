package com.springbootvue.Utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.util.Progressable;
import org.apache.zookeeper.common.IOUtils;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
/**
 * @Author yhz
 * @Data 16:08 2020/1/26
 */
public class HadoopApi {
    //创建全局变量
    FileSystem file=null;
    Configuration cfg=null;
    //创建连接对象的hdfs
    public 	HadoopApi() throws IOException, InterruptedException, URISyntaxException {
        cfg=new Configuration();
        cfg.set("fs.defaultFS", "hdfs://39.107.120.200:8020");
        cfg.set("dfs.client.use.datanode.hostname", "true");
        file=FileSystem.get(cfg);
    }

    //创建创建文件夹
    public  void create() throws IllegalArgumentException, IOException {
        file.mkdirs(new Path("/JAVA_TEST"));
    }
    /***
     * 创建文件的方法
     * @throws IOException
     * @throws IllegalArgumentException
     * */
    public void creat () throws IllegalArgumentException, IOException {
        FSDataOutputStream os = file.create(new Path("/Healthy/Log_2020_01_30.log"));
        os.write("This is my asasasas ".getBytes());
        os.flush();
        os.close();

    }

    /**
     * 读取文件内容
     * @throws IOException
     * @throws IllegalArgumentException
     * */
    public  void look() throws IllegalArgumentException, IOException {
        FSDataInputStream in = file.open(new Path("/JAVA_TEST/Test.txt"));
        IOUtils.copyBytes(in, System.out, 1024);
        System.out.println("读取完毕！！");
        in.close();
    }


    /**
     * 写入文件
     * @throws IOException
     * @throws IllegalArgumentException
     * */
    public  void writeFile() throws IllegalArgumentException, IOException {
        String hdfs_path = "/Healthy/b.log";//文件路径
        Configuration conf = new Configuration();
        conf.setBoolean("dfs.support.append", true);

        String inpath = "/home/wyp/append.txt";
        FileSystem fs = null;
        try {
            fs = FileSystem.get(URI.create(hdfs_path), conf);
            //要追加的文件流，inpath为文件
            InputStream in = new
                    BufferedInputStream(new FileInputStream(inpath));
            OutputStream out = fs.append(new Path(hdfs_path));
            IOUtils.copyBytes(in, out, 4096, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 重命名
     * @throws IOException

     **/
    public void rename() throws IOException {
        Path oldname=new Path("/JAVA_TEST/Test.txt");
        Path newname = new Path("/JAVA_TEST/Test_new.txt");
        file.rename(oldname, newname);
        System.out.println("重命名执行完毕!");
    }
    /**
     * 上传文件
     * @throws IOException
     * **/
    public void LocationToHdfs(String localPath,String hdfsPath) throws IOException {
        Path localpath = new Path(localPath);
        Path hafspath = new Path(hdfsPath);
        file.copyFromLocalFile(localpath, hafspath);
        System.out.println("无进度条上传完成");
    }

    /**
     * 有进度条上传
     * @throws IOException
     * @throws IllegalArgumentException
     *
     *
     * */
    public void load() throws IllegalArgumentException, IOException {
        FileInputStream fin = new FileInputStream(new File("D:\\pig.jpg"));
        BufferedInputStream in = new BufferedInputStream(fin);
        FSDataOutputStream out = file.create(new Path("/JAVA_TEST/pig_2.jpg"),new Progressable() {

            public void progress() {
                // TODO Auto-generated method stub
                System.out.print("*");
            }
        });
        IOUtils.copyBytes(in, out, 4096);
    }

    /**
     * 下载文件
     * @throws IOException
     * **/
    public void HdfsToLocal() throws IOException {
        //使用hadoop本地环境
        file.copyToLocalFile(new Path("/quanfeihu/test.txt"), new Path("C:\\"));
        //不使用hadoop本地环境
        file.copyToLocalFile(false, new Path("/quanfeihu/test.txt"), new Path("C:\\"),true);
    }

    /**
     * 列表输出
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws FileNotFoundException
     * */
    public void showlist() throws FileNotFoundException, IllegalArgumentException, IOException {
        FileStatus[] listStatus = file.listStatus(new Path("/"));
        for(FileStatus list:listStatus) {
            String isdir=list.isDirectory()?"文件夹":"文件";
            long len = list.getLen();
            Path path2 = list.getPath();
            short replication = list.getReplication();
            System.out.println("文件类型:"+isdir+"文件大小"+len+"冗余数"+replication+"文件路径"+path2);
        }

    }
    /***
     * 删除文件
     * @throws IOException
     * */
    public void del() throws IOException {
        Path path3 = new Path("/JAVA_TEST/pig_2.jpg");
        file.delete(path3,true);
        System.out.println("删除完毕");
    }

    /***
     * 判断文件是否存在
     * @throws IOException
     * */
    public void check() throws IOException {
        boolean exist = file.exists(new Path("/quanfeihu/hdfs_my_start.sh"));
    }

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        HadoopApi hd = new HadoopApi();
        hd.showlist();
    }
}
