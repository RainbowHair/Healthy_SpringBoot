package com.springbootvue.Utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author yhz
 * @Data 22:31 2020/2/7
 */
public class T {
    public static void main(String[] args) {
        //模拟数据
        HashMap map1 = new HashMap();
        map1.put("name","yang");
        map1.put("age","22");
        HashMap map2 = new HashMap();
        map2.put("name","zhang");
        map2.put("age","12");
        List<Map> maps = new ArrayList<>();
        maps.add(map1);
        maps.add(map2);
        //输出
        System.out.println("List<Map>:      "+maps);
        //转json
        String string_json = JSON.toJSONString(maps);
        System.out.println("JSON:      "+string_json);
        System.out.println("Map1:"+map1.get("name"));

    }
}
