package com.springbootvue.CommonController;

import com.alibaba.fastjson.JSON;
import com.springbootvue.service.CommonService.CommonService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:32 2020/1/21
 */
@RestController
@RequestMapping("/member")
public class CommonController {
    @Resource
    CommonService service;

    @ApiOperation(value = "会员信息查询接口",notes = "查询")
    @ApiImplicitParam(name = "user",value = "user测试",required = true,dataType = "Map")
    @RequestMapping(value = "/{page}",method = RequestMethod.GET)
    public String select(@PathVariable Map map){
        System.out.println("Map"+map);
        List<Map> select = service.select(map);
        return JSON.toJSONString(select);
    }

    @ApiOperation(value = "会员信息删除接口",notes = "删除")
    @ApiImplicitParam(name = "user",value = "user测试",required = true,dataType = "Map")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void del(@PathVariable Map map){
        service.del(map);
    }
}
