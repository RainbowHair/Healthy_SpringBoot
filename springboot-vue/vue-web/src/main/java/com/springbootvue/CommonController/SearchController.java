package com.springbootvue.CommonController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.springbootvue.Utils.ESApi;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.common.MacAddressProvider;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:04 2020/1/25
 */
@RestController
@RequestMapping("/search")
public class SearchController {

    @ApiOperation(value = "ES条件查询接口",notes = "查询")
    @ApiImplicitParam(name = "user",value = "user测试",required = true,dataType = "Map")
    @RequestMapping(value = "/{name}",method = RequestMethod.GET)
    public String search(@PathVariable Map map) throws IOException {
        ESApi esApi = new ESApi();
        String temp="";
        SearchHits yhz = esApi.search2("Member_Name",map.get("name").toString());
        SearchHit[] hits = yhz.getHits();
        for (SearchHit hit : hits) {
            String source = hit.getSourceAsString();
            temp=source;
        }
        return "["+temp+"]";
    }
}
