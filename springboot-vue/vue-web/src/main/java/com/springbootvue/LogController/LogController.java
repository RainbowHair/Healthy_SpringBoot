package com.springbootvue.LogController;

import com.alibaba.fastjson.JSON;
import com.springbootvue.Utils.HadoopApi;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 17:47 2020/1/27
 */
@RestController
@RequestMapping("/log")
public class LogController {

    @ApiOperation(value = "日志上传接口",notes = "上传")
    @ApiImplicitParam(name = "user",value = "user测试",required = true,dataType = "Map")
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public void log() throws InterruptedException, IOException, URISyntaxException {
        HadoopApi api = new HadoopApi();
        Date date = new Date();
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        String flag = simpleDateFormat2.format(date);
        api.LocationToHdfs("/root/HealthyLog/"+flag+".log","/Healthy/Log/"+flag+".log");
    }
}
