package com.springbootvue.UserController;

import com.alibaba.fastjson.JSON;
import com.springbootvue.service.UserService.UserService;
import com.springbootvue.service.UserService.UserServiceConsumer;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 11:16 2020/2/2
 */
@RestController
@RequestMapping("/userinfo")
public class UserController {

    @Resource
    UserServiceConsumer service;

    //Test
    @ApiOperation(value = "预约管理列表查询接口")
    @RequestMapping(value = "/{page}",method = RequestMethod.GET)
    public String userinfo(@PathVariable Map map2) throws IOException {
        return service.invok2(map2);
    }
}
