package com.springbootvue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@MapperScan(basePackages = {"com.springbootvue.dao","com.springbootvue.security.mapper"})
@SpringBootApplication
public class SpringbootVueApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SpringbootVueApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringbootVueApplication.class, args);
    }
}
