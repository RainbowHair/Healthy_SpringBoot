package com.springbootvue.dao.CommonDao;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:24 2020/1/21
 */
public interface CommonMapper {
    //查询会员信息
    public List<Map> select(Map map);
    //删除会员信息
    public List<Map> del(Map map);
}
