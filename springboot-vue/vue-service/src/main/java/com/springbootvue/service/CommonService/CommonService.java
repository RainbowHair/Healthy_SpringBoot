package com.springbootvue.service.CommonService;

import com.springbootvue.dao.CommonDao.CommonMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 14:26 2020/1/21
 */
@Service
public class CommonService implements CommonMapper {

    @Autowired
    SqlSessionTemplate sqlSession;

    //查询会员信息
    @Override
    public List<Map> select(Map map) { return sqlSession.selectList("com.springbootvue.dao.CommonDao.CommonMapper.select",map);}
    //删除会员信息
    @Override
    public List<Map> del(Map map) {  return sqlSession.selectList("com.springbootvue.dao.CommonDao.CommonMapper.del",map); }
}
