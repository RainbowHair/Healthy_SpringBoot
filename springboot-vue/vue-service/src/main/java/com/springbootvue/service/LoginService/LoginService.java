package com.springbootvue.service.LoginService;

import com.springbootvue.dao.LoginDao.LoginMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author yhz
 * @Data 20:11 2020/1/24
 */
@Service
public class LoginService implements LoginMapper {
    @Autowired
    SqlSessionTemplate sqlSession;
    //登陆
    @Override
    public Map login(Map map) { return sqlSession.selectOne("com.springbootvue.dao.LoginDao.LoginMapper.login",map); }

}
