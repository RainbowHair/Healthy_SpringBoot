package com.springbootvue.service.UserService;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @Author yhz
 * @Data 11:43 2020/2/2
 */
@Component
public class UserServiceConsumer {

    @Reference(version = "1.0")
    UserService service;

    //查询用户信息
    public String invok2(Map map){ return JSON.toJSONString(service.select(map));}
}
