package com.springbootvue.service.UserService;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 11:10 2020/2/2
 */
public interface UserService {
    //查询用户信息
    public List<Map> select(Map map);
    //Test
    String hello(String name);
}