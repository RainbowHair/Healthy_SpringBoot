package com.springbootvue.DemoController;

import com.alibaba.fastjson.JSON;
import com.springbootvue.service.UserService.UserServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;

/**
 * @Author yhz
 * @Data 12:35 2020/2/2
 */
@RestController
@RequestMapping("/qq")
public class DemoControlelr {

    @Resource
    UserServiceImpl service;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String search() throws IOException {
        HashMap map = new HashMap();
        map.put("page","1");
        return JSON.toJSONString(service.select(map));
    }

}
