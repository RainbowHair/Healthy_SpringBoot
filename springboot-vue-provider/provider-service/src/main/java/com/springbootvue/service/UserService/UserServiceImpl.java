package com.springbootvue.service.UserService;

import com.alibaba.dubbo.config.annotation.Service;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;


/**
 * @Author yhz
 * @Data 11:10 2020/2/2
 */
@Service(version = "1.0")
public class UserServiceImpl implements UserService {
    @Autowired
    SqlSessionTemplate sqlSession;

    //查询用户信息
    public List<Map> select(Map map) { return sqlSession.selectList("com.springbootvue.dao.UserDao.UserMapper.select",map); }

    //Test
    public String hello(String name) {
        return "I Love "+name;
    }

}
