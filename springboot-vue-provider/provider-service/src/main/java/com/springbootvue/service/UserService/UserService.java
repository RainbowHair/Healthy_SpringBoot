package com.springbootvue.service.UserService;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 11:08 2020/2/2
 */
public interface UserService {
    //查询用户信息
    public List<Map> select(Map map);
    //Test
    String hello(String name);
}
