package com.springbootvue.dao.UserDao;

import java.util.List;
import java.util.Map;

/**
 * @Author yhz
 * @Data 11:08 2020/2/2
 */
public interface UserMapper {
    //查询用户信息
    public List<Map> select(Map map);
}
