import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from "../components/HelloWorld"
import second from "../components/views/second"
import Confirm from "../components/sub/Confirm"
import BootStrapTest from "../components/Test/BootStrapTest"
import Show from "../components/views/Show";
import Test from "../components/views/Test";
import MemberInfo from "../components/views/MemberInfo";
import Index from "../components/views/Index";
import Subscribe from "../components/views/Subscribe";
import Login from "../components/views/Login";
Vue.use(Router)

export default new Router({
	base: '/',
    mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/second',
      name: 'second',
      component: second
    },
    {
      path: '/confirm',
      name: 'second',
      component: Confirm
    },
    {
      path: '/boot',
      name: 'BootStrapTest',
      component: BootStrapTest
    }
    ,
    {
      path: '/show',
      name: 'Show',
      component: Show
    }
    ,
    {
      path: '/test',
      name: 'Test',
      component: Test
    },
    {
      path: '/MemberInfo',
      name: 'MemberInfo',
      component: MemberInfo
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/Subscribe',
      name: 'Subscribe',
      component: Subscribe
    }
  ]
})

